// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';

import 'package:training_timer/main.dart';
import 'package:training_timer/providers/main_provider.dart';

void main() {
  testWidgets('Decremet seconds', (WidgetTester tester) async {
    var mainProvider = MainProvider();
    mainProvider.setCurrentTimer(1, 30);
    mainProvider.decrementCurrentTimer();
    expect(mainProvider.currentSecondTimer, 29);
  });

  testWidgets('only left seconds', (WidgetTester tester) async {
    var mainProvider = MainProvider();
    mainProvider.setCurrentTimer(1, 0);
    mainProvider.decrementCurrentTimer();
    expect(mainProvider.currentSecondTimer, 59);
    expect(mainProvider.currentMinuteTimer, 0);
  });

  testWidgets('total seconds', (WidgetTester tester) async {
    var mainProvider = MainProvider();
    mainProvider.setCurrentTimer(1, 30);
    expect(mainProvider.currentMaxTimeInSeconds, 90);
  });
}
