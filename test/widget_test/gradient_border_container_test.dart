import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:training_timer/general_components/gradient_border_container.dart';

import 'package:training_timer/main.dart';

void main() {
  Widget makeTestableWidget({Widget child}) {
    return MediaQuery(
      data: MediaQueryData(),
      child: MaterialApp(
        home: SingleChildScrollView(
          child: Column(
            children: [child, child, child],
          ),
        ),
      ),
    );
  }

  testWidgets('pump widget', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    GradientBorderContainer container = GradientBorderContainer(
      backgroundColor: Colors.black,
      borderColor: LinearGradient(colors: [Colors.black, Colors.amberAccent]),
      child: SizedBox(width: 100, height: 100),
      height: 100,
    );

    await tester.pumpWidget(makeTestableWidget(child: container));
    expect(tester.takeException(), isNull);
  });

  testWidgets('different height', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    GradientBorderContainer container = GradientBorderContainer(
      backgroundColor: Colors.black,
      borderColor: LinearGradient(colors: [Colors.black, Colors.amberAccent]),
      child: SizedBox(width: 100, height: 100),
      height: 0,
    );

    await tester.pumpWidget(makeTestableWidget(child: container));
    expect(tester.takeException(), isNull);
  });

  testWidgets('border color null', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    GradientBorderContainer container = GradientBorderContainer(
      backgroundColor: Colors.black,
      borderColor: null,
      child: SizedBox(width: 100, height: 100),
      height: 0,
    );

    await tester.pumpWidget(makeTestableWidget(child: container));
    expect(tester.takeException(), isNull);
  });
}
