import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:training_timer/general_components/gradient_border_container.dart';
import 'package:training_timer/general_components/inc_dec_button.dart';

void main() {
  Widget makeTestableWidget({Widget child}) {
    return MediaQuery(
      data: MediaQueryData(),
      child: MaterialApp(
        home: Scaffold(
          body: SingleChildScrollView(
            child: Column(
              children: [
                Material(
                  child: SizedBox(child: child, width: 100, height: 100),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  testWidgets('increment', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    int quantity = 0;
    IncDecButton button = IncDecButton(
        decButtonKey: Key("dec"),
        incButtonKey: Key("inc"),
        backgroundColor: Colors.black,
        iconColor: Colors.white,
        text: quantity.toString(),
        style: TextStyle(color: Colors.grey, fontSize: 25),
        borderColor: LinearGradient(colors: [Colors.red, Colors.white]),
        onTapMinus: () {
          if (quantity > 0) {
            quantity--;
          }
        },
        onTapPlus: () {
          if (quantity < 9) {
            quantity++;
          }
        });
    await tester.pumpWidget(makeTestableWidget(child: button));
    await tester.tap(find.byKey(Key("inc")));
    expect(quantity, 1);
  });

  testWidgets('decrement', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    int quantity = 9;
    IncDecButton button = IncDecButton(
        decButtonKey: Key("dec"),
        incButtonKey: Key("inc"),
        backgroundColor: Colors.black,
        iconColor: Colors.white,
        text: quantity.toString(),
        style: TextStyle(color: Colors.grey, fontSize: 25),
        borderColor: LinearGradient(colors: [Colors.red, Colors.white]),
        onTapMinus: () {
          if (quantity > 0) {
            quantity--;
          }
        },
        onTapPlus: () {
          if (quantity < 9) {
            quantity++;
          }
        });
    await tester.pumpWidget(makeTestableWidget(child: button));
    await tester.tap(find.byKey(Key("dec")));
    await tester.tap(find.byKey(Key("dec")));
    expect(quantity, 7);
  });
}
