import 'package:flutter/foundation.dart';
import 'package:provider/provider.dart';
import 'package:training_timer/models/routine_item_model.dart';
import 'package:training_timer/models/routine_model.dart';

class MainProvider with ChangeNotifier, DiagnosticableTreeMixin {
  int _preparationMinutes = 0;
  int _preparationSeconds = 0;
  int _trainingMinutes = 0;
  int _trainingSeconds = 0;
  int _restMinutes = 0;
  int _restSeconds = 0;
  int _restBetweenCircuitMinutes = 0;
  int _restBetweenCircuitSeconds = 0;
  int _circuitQuantity = 0;
  int _exercisePerCircuit = 0;
  RoutineModel _routine = null;
  List<RoutineModel> _routines = [];
  int _currentMinuteTimer = 0;
  int _currentSecondTimer = 0;
  double _currentMaxTimeInSeconds = 0;
  double _currentProgressInSeconds = 0;
  String _currentStage = "Preparacion";

  int get preparationMinutes => _preparationMinutes;
  int get preparationSeconds => _preparationSeconds;
  int get trainingMinutes => _trainingMinutes;
  int get trainingSeconds => _trainingSeconds;
  int get restMinutes => _restMinutes;
  int get restSeconds => _restSeconds;
  int get circuitQuantity => _circuitQuantity;
  int get exercisePerCircuit => _exercisePerCircuit;
  int get restBetweenCircuitMinutes => _restBetweenCircuitMinutes;
  int get restBetweenCircuitSeconds => _restBetweenCircuitSeconds;
  int get currentMinuteTimer => _currentMinuteTimer;
  int get currentSecondTimer => _currentSecondTimer;
  double get currentMaxTimeInSeconds => _currentMaxTimeInSeconds;
  double get currentProgressInSeconds => _currentProgressInSeconds;
  RoutineModel get routine => _routine;
  List<RoutineModel> get routines => _routines;
  String get currentStage => _currentStage;

  setPreparationTime(int minutes, int seconds) {
    this._preparationMinutes = minutes;
    this._preparationSeconds = seconds;
    notifyListeners();
  }

  setTrainingTime(int minutes, int seconds) {
    this._trainingMinutes = minutes;
    this._trainingSeconds = seconds;
    notifyListeners();
  }

  setRestTime(int minutes, int seconds) {
    this._restMinutes = minutes;
    this._restSeconds = seconds;
    notifyListeners();
  }

  setRestBetweenCircuitTime(int minutes, int seconds) {
    this._restBetweenCircuitMinutes = minutes;
    this._restBetweenCircuitSeconds = seconds;
    notifyListeners();
  }

  setCircuitQuantity(int quantity) {
    this._circuitQuantity = quantity;
    notifyListeners();
  }

  setExercisePerCircuit(int quantity) {
    this._exercisePerCircuit = quantity;
    notifyListeners();
  }

  decrementCurrentTimer() {
    if (currentSecondTimer != 0) {
      _currentSecondTimer--;
    } else {
      if (currentMinuteTimer != 0) {
        _currentMinuteTimer--;
        _currentSecondTimer = 59;
      }
    }
    _currentProgressInSeconds++;
    notifyListeners();
  }

  setCurrentTimer(int minutes, int seconds) {
    this._currentMinuteTimer = minutes;
    this._currentSecondTimer = seconds;
    this._currentMaxTimeInSeconds = (minutes * 60.0) + seconds;
    this._currentProgressInSeconds = 0.0;
    notifyListeners();
  }

  setCurrentStage(String stage) {
    _currentStage = stage;
  }

  decrementPreparationTime() {
    if (preparationSeconds != 0) {
      _preparationSeconds--;
    } else {
      if (preparationMinutes != 0) {
        _preparationMinutes--;
        _preparationSeconds = 59;
      }
    }
    notifyListeners();
  }

  decrementTrainingTime() {
    if (trainingSeconds != 0) {
      _trainingSeconds--;
    } else {
      if (trainingMinutes != 0) {
        _trainingMinutes--;
        _trainingSeconds = 59;
      }
    }
    notifyListeners();
  }

  decrementRestTime() {
    if (restSeconds != 0) {
      _restSeconds--;
    } else {
      if (restMinutes != 0) {
        _restMinutes--;
        _restSeconds = 59;
      }
    }
    notifyListeners();
  }

  setRoutine(RoutineModel routine) {
    this._routine = routine;
    notifyListeners();
  }

  addRoutineItem(RoutineItemModel routineItem) {
    if (this._routine != null) {
      this._routine.routineItems.add(routineItem);
      this._exercisePerCircuit++;
    }
    notifyListeners();
  }

  removeLastRoutineItem() {
    if (this._routine != null) {
      this._routine.routineItems.removeLast();
      this._exercisePerCircuit--;
    }
    notifyListeners();
  }

  updateRoutineItem(RoutineItemModel routineItem, int index) {
    if (this._routine != null) {
      this._routine.routineItems[index].restMinutes = routineItem.restMinutes;
      this._routine.routineItems[index].restSeconds = routineItem.restSeconds;
      this._routine.routineItems[index].trainingMinutes =
          routineItem.trainingMinutes;
      this._routine.routineItems[index].trainingSeconds =
          routineItem.trainingSeconds;
    }
    notifyListeners();
  }

  setRoutineList() {
    if (circuitQuantity == 0) {
      _routines.add(_routine);
    } else {
      for (int i = 0; i <= _circuitQuantity - 1; i++) {
        _routines.add(_routine);
      }
    }
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(IntProperty('preparationMinutes', _preparationMinutes));
    properties.add(IntProperty('preparationSeconds', _preparationSeconds));
    properties.add(IntProperty('trainingMinutes', _trainingMinutes));
    properties.add(IntProperty('trainingSeconds', _trainingSeconds));
    properties.add(IntProperty('restMinutes', _restMinutes));
    properties.add(IntProperty('restSeconds', _restSeconds));
    properties.add(IntProperty('circuitQuantity', _circuitQuantity));
    properties.add(IntProperty('exercisePerCircuit', _exercisePerCircuit));
  }
}
