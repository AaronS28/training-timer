import 'dart:ui';

Color k_black = const Color(0xFF000000);
Color k_neonBlack = const Color(0xFF252525);
Color k_neonViolete = const Color(0xFF6930C3);
Color k_neonGreen = const Color(0xFF64DFDF);
Color k_neonLightGreen = const Color(0xFF80FFDB);
