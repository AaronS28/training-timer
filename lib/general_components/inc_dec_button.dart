import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:training_timer/constants/k_colors.dart';
import 'package:training_timer/general_components/gradient_border_container.dart';

class IncDecButton extends StatefulWidget {
  Color backgroundColor;
  Color iconColor;
  LinearGradient borderColor;
  String text;
  TextStyle style;
  Function onTapPlus;
  Function onTapMinus;
  Key incButtonKey;
  Key decButtonKey;

  IncDecButton(
      {this.incButtonKey,
      this.decButtonKey,
      this.backgroundColor,
      this.borderColor,
      this.iconColor,
      this.text,
      this.style,
      this.onTapMinus,
      this.onTapPlus});

  @override
  _IncDecButtonState createState() => _IncDecButtonState();
}

class _IncDecButtonState extends State<IncDecButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: k_black,
      child: Column(
        children: [
          Expanded(
            child: InkWell(
              key: widget.incButtonKey == null
                  ? UniqueKey()
                  : widget.incButtonKey,
              child: GradientBorderContainer(
                backgroundColor: widget.backgroundColor,
                borderColor: widget.borderColor,
                child: Image.asset(
                  'images/plus.png',
                  color: widget.iconColor,
                ),
              ),
              onTap: () {
                widget.onTapPlus();
              },
            ),
          ),
          Expanded(
            child: Container(
              color: widget.backgroundColor,
              child: Center(child: Text(widget.text, style: widget.style)),
            ),
          ),
          Expanded(
            child: InkWell(
              key: widget.decButtonKey == null
                  ? UniqueKey()
                  : widget.decButtonKey,
              child: GradientBorderContainer(
                backgroundColor: widget.backgroundColor,
                borderColor: widget.borderColor,
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Image.asset(
                    'images/minus.png',
                    color: widget.iconColor,
                  ),
                ),
              ),
              onTap: () {
                widget.onTapMinus();
              },
            ),
          ),
        ],
      ),
    );
  }
}
