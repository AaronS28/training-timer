import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:training_timer/constants/k_colors.dart';

class GradientBorderContainer extends StatefulWidget {
  Color backgroundColor;
  LinearGradient borderColor;
  Widget child;
  double height;

  GradientBorderContainer({
    this.backgroundColor,
    this.borderColor,
    this.child,
    this.height,
  });

  @override
  _GradientBorderContainerState createState() =>
      _GradientBorderContainerState();
}

class _GradientBorderContainerState extends State<GradientBorderContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: widget.height,
        padding: EdgeInsets.all(3),
        decoration: BoxDecoration(
            color: widget.backgroundColor,
            gradient: LinearGradient(colors: [k_neonViolete, k_neonGreen]),
            borderRadius: BorderRadius.circular(10)),
        child: Container(
            decoration: BoxDecoration(
                color: widget.backgroundColor,
                borderRadius: BorderRadius.circular(10)),
            child: widget.child));
  }
}
