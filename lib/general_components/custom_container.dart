import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:training_timer/constants/k_colors.dart';

class CustomContainer extends StatefulWidget {
  String title;
  String subTitle;
  Function onTap;
  Color backgroundColor;
  LinearGradient borderColor;
  TextStyle textStyle;
  IconData icon;
  double height;
  double width;

  CustomContainer({
    this.title,
    this.subTitle,
    this.onTap,
    this.backgroundColor,
    this.borderColor,
    this.textStyle,
    this.icon,
    this.height = 100,
    this.width = 100,
  });

  @override
  _CustomContainerState createState() => _CustomContainerState();
}

class _CustomContainerState extends State<CustomContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.width,
      height: widget.height,
      padding: EdgeInsets.all(3),
      decoration: BoxDecoration(
          color: widget.backgroundColor,
          gradient: LinearGradient(colors: [k_neonViolete, k_neonGreen]),
          borderRadius: BorderRadius.circular(10)),
      child: Container(
        decoration: BoxDecoration(
            color: widget.backgroundColor,
            borderRadius: BorderRadius.circular(10)),
        child: InkWell(
          onTap: () {
            widget.onTap();
          },
          child: Row(
            children: [
              Expanded(
                child: SizedBox(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(widget.title, style: widget.textStyle),
                        Text(widget.subTitle, style: widget.textStyle)
                      ]),
                ),
              ),
              Icon(
                widget.icon,
                color: Colors.black,
              )
            ],
          ),
        ),
      ),
    );
  }
}
