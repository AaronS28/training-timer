import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:training_timer/builder/RoutineBuilder.dart';
import 'package:training_timer/models/routine_model.dart';
import 'package:training_timer/popups/quantity_popup.dart';
import 'package:training_timer/popups/timer_popup.dart';
import 'package:training_timer/providers/main_provider.dart';

class RoutineScreenLogic {
  static callPreparationTimePopUp(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return TimerPopup(
          title: "Preaparacion",
          onAccept: (int minutes, int seconds) {
            Provider.of<MainProvider>(context, listen: false)
                .setPreparationTime(minutes, seconds);
          },
        );
      },
    );
  }

  static callTrainingTimePopUp(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return TimerPopup(
          title: "Ejercicio",
          onAccept: (int minutes, int seconds) {
            Provider.of<MainProvider>(context, listen: false)
                .setTrainingTime(minutes, seconds);
          },
        );
      },
    );
  }

  static callRestTimePopUp(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return TimerPopup(
          title: "Descanso",
          onAccept: (int minutes, int seconds) {
            Provider.of<MainProvider>(context, listen: false)
                .setRestTime(minutes, seconds);
          },
        );
      },
    );
  }

  static callExerciseQuantityPopUp(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return QuantityPopup(
          title: "Ejercicios",
          onAccept: (int quantity) {
            Provider.of<MainProvider>(context, listen: false)
                .setExercisePerCircuit(quantity);
          },
        );
      },
    );
  }

  static callCircuitQuantityPopUp(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return QuantityPopup(
          title: "Circuitos",
          onAccept: (int quantity) {
            Provider.of<MainProvider>(context, listen: false)
                .setCircuitQuantity(quantity);
          },
        );
      },
    );
  }

  static onCreateCustomRoutineTap(BuildContext context) {
    RoutineModel result =
        Provider.of<MainProvider>(context, listen: false).routine;
    if (result == null) {
      result = RoutineScreenLogic.callRoutineBuilder(context);
      Provider.of<MainProvider>(context, listen: false).setRoutine(result);
    }
  }

  static callRestTimeBetweenCircuitPopUp(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return TimerPopup(
          title: "Descanso Entre Circuitos",
          onAccept: (int minutes, int seconds) {
            Provider.of<MainProvider>(context, listen: false)
                .setRestBetweenCircuitTime(minutes, seconds);
          },
        );
      },
    );
  }

  static RoutineModel callRoutineBuilder(context) {
    return RoutineBuilder().buildRoutine(
        restMinutes:
            Provider.of<MainProvider>(context, listen: false).restMinutes,
        restSeconds:
            Provider.of<MainProvider>(context, listen: false).restSeconds,
        trainingMinutes:
            Provider.of<MainProvider>(context, listen: false).trainingMinutes,
        trainingSeconds:
            Provider.of<MainProvider>(context, listen: false).trainingSeconds,
        exerciseQuantity: Provider.of<MainProvider>(context, listen: false)
            .exercisePerCircuit,
        restRoutineMinutes: Provider.of<MainProvider>(context, listen: false)
            .restBetweenCircuitMinutes,
        restRoutineSeconds: (Provider.of<MainProvider>(context, listen: false)
                    .restBetweenCircuitSeconds !=
                0)
            ? Provider.of<MainProvider>(context, listen: false)
                .restBetweenCircuitSeconds
            : 30);
  }
}
