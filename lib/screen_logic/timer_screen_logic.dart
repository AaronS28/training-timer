import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:training_timer/providers/main_provider.dart';
import 'package:audioplayers/audioplayers.dart';

const String bell = "boxing_bell_sound_effect.mp3";
const String explosion = "explosion_sound_effect.mp3";

class TimerScreenLogic {
  static startPreparationTime(BuildContext context) async {
    AudioCache player = new AudioCache();
    Provider.of<MainProvider>(context, listen: false)
        .setCurrentStage("Preparacion");
    var minutes =
        Provider.of<MainProvider>(context, listen: false).preparationMinutes;
    var seconds =
        Provider.of<MainProvider>(context, listen: false).preparationSeconds;
    Provider.of<MainProvider>(context, listen: false)
        .setCurrentTimer(minutes, seconds);
    while (Provider.of<MainProvider>(context, listen: false)
                .currentMinuteTimer !=
            0 ||
        Provider.of<MainProvider>(context, listen: false).currentSecondTimer !=
            0) {
      await Future.delayed(Duration(seconds: 1));
      Provider.of<MainProvider>(context, listen: false).decrementCurrentTimer();
      sendNotif(context, "Preparacion");
    }
    await player.play(bell);
  }

  static startTraining(BuildContext context) async {
    for (int i = 0;
        i <=
            Provider.of<MainProvider>(context, listen: false).routines.length -
                1;
        i++) {
      if (i != 0) {
        await _startRestBetweenCircuitTime(i, context);
      }
      for (int j = 0;
          j <=
              Provider.of<MainProvider>(context, listen: false)
                      .routines[i]
                      .routineItems
                      .length -
                  1;
          j++) {
        await _startTrainingTime(i, j, context);
        await _startRestTime(i, j, context);
      }
    }
  }

  static _startRestBetweenCircuitTime(int index, BuildContext context) async {
    Provider.of<MainProvider>(context, listen: false)
        .setCurrentStage("Descanso entre circuitos");
    var minutes = Provider.of<MainProvider>(context, listen: false)
        .routines[index]
        .restRoutineMinutes;
    var seconds = Provider.of<MainProvider>(context, listen: false)
        .routines[index]
        .restRoutineSeconds;
    Provider.of<MainProvider>(context, listen: false)
        .setCurrentTimer(minutes, seconds);
    while (Provider.of<MainProvider>(context, listen: false)
                .currentMinuteTimer !=
            0 ||
        Provider.of<MainProvider>(context, listen: false).currentSecondTimer !=
            0) {
      await Future.delayed(Duration(seconds: 1));
      Provider.of<MainProvider>(context, listen: false).decrementCurrentTimer();
      sendNotif(context, "Descanso entre circuitos");
    }
  }

  static _startTrainingTime(
      int index, int subIndex, BuildContext context) async {
    AudioCache player = new AudioCache();

    Provider.of<MainProvider>(context, listen: false)
        .setCurrentStage("Ejercicio");
    var minutes = Provider.of<MainProvider>(context, listen: false)
        .routines[index]
        .routineItems[subIndex]
        .trainingMinutes;
    var seconds = Provider.of<MainProvider>(context, listen: false)
        .routines[index]
        .routineItems[subIndex]
        .trainingSeconds;
    Provider.of<MainProvider>(context, listen: false)
        .setCurrentTimer(minutes, seconds);

    while (Provider.of<MainProvider>(context, listen: false)
                .currentMinuteTimer !=
            0 ||
        Provider.of<MainProvider>(context, listen: false).currentSecondTimer !=
            0) {
      await Future.delayed(Duration(seconds: 1));
      Provider.of<MainProvider>(context, listen: false).decrementCurrentTimer();
      sendNotif(context, "Ejercicio");
    }
    await player.play(explosion);
  }

  static _startRestTime(int index, int subIndex, BuildContext context) async {
    Provider.of<MainProvider>(context, listen: false)
        .setCurrentStage("Descanso");
    AudioCache player = new AudioCache();
    var minutes = Provider.of<MainProvider>(context, listen: false)
        .routines[index]
        .routineItems[subIndex]
        .restMinutes;
    var seconds = Provider.of<MainProvider>(context, listen: false)
        .routines[index]
        .routineItems[subIndex]
        .restSeconds;
    Provider.of<MainProvider>(context, listen: false)
        .setCurrentTimer(minutes, seconds);
    while (Provider.of<MainProvider>(context, listen: false)
                .currentMinuteTimer !=
            0 ||
        Provider.of<MainProvider>(context, listen: false).currentSecondTimer !=
            0) {
      await Future.delayed(Duration(seconds: 1));
      Provider.of<MainProvider>(context, listen: false).decrementCurrentTimer();
      sendNotif(context, "Descanso");
    }
    await player.play(bell);
  }

  static sendNotif(BuildContext context, String stage) {
    AwesomeNotifications().createNotification(
        content: NotificationContent(
            id: 10,
            channelKey: 'media_player',
            title: 'Tiempo de ' + stage,
            body:
                '${Provider.of<MainProvider>(context, listen: false).currentMinuteTimer}:${Provider.of<MainProvider>(context, listen: false).currentSecondTimer}'));
  }
}
