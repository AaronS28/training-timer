import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:training_timer/builder/RoutineBuilder.dart';
import 'package:training_timer/models/routine_item_model.dart';
import 'package:training_timer/models/routine_model.dart';
import 'package:training_timer/popups/timer_popup.dart';
import 'package:training_timer/providers/main_provider.dart';

class RoutineScreenDetailLogic {
  static onCancelCustomRoutineConfig(
      BuildContext context, oldExceriseQuantity) {
    RoutineModel result = RoutineBuilder().buildRoutine(
        restMinutes:
            Provider.of<MainProvider>(context, listen: false).restMinutes,
        restSeconds:
            Provider.of<MainProvider>(context, listen: false).restSeconds,
        trainingMinutes:
            Provider.of<MainProvider>(context, listen: false).trainingMinutes,
        trainingSeconds:
            Provider.of<MainProvider>(context, listen: false).trainingSeconds,
        exerciseQuantity: oldExceriseQuantity,
        restRoutineMinutes: 0,
        restRoutineSeconds: 30);
    Provider.of<MainProvider>(context, listen: false).setRoutine(result);
    Provider.of<MainProvider>(context, listen: false)
        .setExercisePerCircuit(oldExceriseQuantity);
  }

  static callTrainingTimePopUp(BuildContext context, int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return TimerPopup(
          title: "Ejercicio",
          onAccept: (int minutes, int seconds) {
            int restMinutes = Provider.of<MainProvider>(context, listen: false)
                .routine
                .routineItems[index]
                .restMinutes;
            int restSeconds = Provider.of<MainProvider>(context, listen: false)
                .routine
                .routineItems[index]
                .restSeconds;
            Provider.of<MainProvider>(context, listen: false).updateRoutineItem(
                new RoutineItemModel(
                    restMinutes: restMinutes,
                    restSeconds: restSeconds,
                    trainingMinutes: minutes,
                    trainingSeconds: seconds),
                index);
          },
        );
      },
    );
  }

  static callRestTimePopUp(BuildContext context, int index) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return TimerPopup(
          title: "Descanso",
          onAccept: (int minutes, int seconds) {
            int trainingMinutes =
                Provider.of<MainProvider>(context, listen: false)
                    .routine
                    .routineItems[index]
                    .trainingMinutes;
            int trainingSeconds =
                Provider.of<MainProvider>(context, listen: false)
                    .routine
                    .routineItems[index]
                    .trainingSeconds;
            Provider.of<MainProvider>(context, listen: false).updateRoutineItem(
                new RoutineItemModel(
                    restMinutes: minutes,
                    restSeconds: seconds,
                    trainingMinutes: trainingMinutes,
                    trainingSeconds: trainingSeconds),
                index);
          },
        );
      },
    );
  }
}
