import 'package:training_timer/models/routine_item_model.dart';
import 'package:training_timer/models/routine_model.dart';

class RoutineBuilder {
  static final RoutineBuilder _singleton = new RoutineBuilder._internal();

  factory RoutineBuilder() {
    return _singleton;
  }

  RoutineBuilder._internal();

  RoutineModel buildRoutine({
    int exerciseQuantity,
    int trainingMinutes,
    int trainingSeconds,
    int restMinutes,
    int restSeconds,
    int restRoutineMinutes,
    int restRoutineSeconds,
  }) {
    var routineItemList = <RoutineItemModel>[];
    for (int i = 0; i <= exerciseQuantity - 1; i++) {
      routineItemList.add(new RoutineItemModel(
          restMinutes: restMinutes,
          restSeconds: restSeconds,
          trainingMinutes: trainingMinutes,
          trainingSeconds: trainingSeconds));
    }

    return RoutineModel(
        restRoutineMinutes: restMinutes,
        restRoutineSeconds: restRoutineSeconds,
        routineItems: routineItemList);
  }
}
