import 'dart:isolate';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:training_timer/providers/main_provider.dart';
import 'package:training_timer/screens/routine_detail_page.dart';
import 'package:training_timer/screens/routine_page.dart';
import 'package:training_timer/screens/timer_page.dart';
import 'package:awesome_notifications/awesome_notifications.dart';

const String isolateName = 'isolate';
final ReceivePort port = ReceivePort();

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  AwesomeNotifications().initialize('', [
    NotificationChannel(
        icon: '',
        channelKey: 'media_player',
        channelName: 'Media player controller',
        channelDescription: 'Media player controller',
        defaultPrivacy: NotificationPrivacy.Public,
        enableVibration: false,
        enableLights: false,
        playSound: false,
        locked: true),
  ]);

  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => MainProvider()),
    ],
    child: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Training Timer',
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes: {
        '/': (context) => RoutinePage(title: ""),
        '/routineDetail': (context) => RoutineDetailPage(title: ""),
        '/timer': (context) => TimerPage(title: ""),
      },
    );
  }
}
