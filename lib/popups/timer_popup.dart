import 'package:flutter/material.dart';
import 'package:training_timer/constants/k_colors.dart';
import 'package:training_timer/general_components/gradient_border_container.dart';
import 'package:training_timer/general_components/inc_dec_button.dart';

class TimerPopup extends StatefulWidget {
  Function(int, int) onAccept;
  String title;

  TimerPopup({
    this.title = "",
    this.onAccept,
  });

  @override
  _TimerPopupState createState() => _TimerPopupState();
}

class _TimerPopupState extends State<TimerPopup> {
  int minuteTens = 0;
  int minuteUnit = 0;
  int secondTens = 0;
  int secondUnit = 0;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: GradientBorderContainer(
        backgroundColor: k_black,
        borderColor: LinearGradient(colors: [k_neonViolete, k_neonGreen]),
        height: 350,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Center(
                  child: Text(
                widget.title,
                style: TextStyle(color: k_neonLightGreen, fontSize: 25),
              )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(children: [
                    Text("Minutos",
                        style:
                            TextStyle(color: k_neonLightGreen, fontSize: 15)),
                    SizedBox(
                      height: 10,
                    ),
                    Row(children: [
                      SizedBox(
                        height: 200,
                        child: IncDecButton(
                            backgroundColor: k_black,
                            iconColor: k_neonLightGreen,
                            text: minuteTens.toString(),
                            style: TextStyle(
                                color: k_neonLightGreen, fontSize: 25),
                            borderColor: LinearGradient(
                                colors: [k_neonViolete, k_neonGreen]),
                            onTapMinus: () {
                              setState(() {
                                if (minuteTens > 0) {
                                  minuteTens--;
                                }
                              });
                            },
                            onTapPlus: () {
                              setState(() {
                                if (minuteTens < 9) {
                                  minuteTens++;
                                }
                              });
                            }),
                      ),
                      SizedBox(
                        height: 200,
                        child: IncDecButton(
                            backgroundColor: k_black,
                            iconColor: k_neonLightGreen,
                            text: minuteUnit.toString(),
                            style: TextStyle(
                                color: k_neonLightGreen, fontSize: 25),
                            borderColor: LinearGradient(
                                colors: [k_neonViolete, k_neonGreen]),
                            onTapMinus: () {
                              setState(() {
                                if (minuteUnit > 0) {
                                  minuteUnit--;
                                }
                              });
                            },
                            onTapPlus: () {
                              setState(() {
                                if (minuteUnit < 9) {
                                  minuteUnit++;
                                }
                              });
                            }),
                      ),
                    ])
                  ]),
                  Column(
                    children: [
                      Text("Segundos",
                          style:
                              TextStyle(color: k_neonLightGreen, fontSize: 15)),
                      SizedBox(
                        height: 10,
                      ),
                      Row(children: [
                        SizedBox(
                          height: 200,
                          child: IncDecButton(
                              backgroundColor: k_black,
                              iconColor: k_neonLightGreen,
                              text: secondTens.toString(),
                              style: TextStyle(
                                  color: k_neonLightGreen, fontSize: 25),
                              borderColor: LinearGradient(
                                  colors: [k_neonViolete, k_neonGreen]),
                              onTapMinus: () {
                                setState(() {
                                  if (secondTens > 0) {
                                    secondTens--;
                                  }
                                });
                              },
                              onTapPlus: () {
                                setState(() {
                                  if (secondTens < 5) {
                                    secondTens++;
                                  }
                                });
                              }),
                        ),
                        SizedBox(
                          height: 200,
                          child: IncDecButton(
                              backgroundColor: k_black,
                              iconColor: k_neonLightGreen,
                              text: secondUnit.toString(),
                              style: TextStyle(
                                  color: k_neonLightGreen, fontSize: 25),
                              borderColor: LinearGradient(
                                  colors: [k_neonViolete, k_neonGreen]),
                              onTapMinus: () {
                                setState(() {
                                  if (secondUnit > 0) {
                                    secondUnit--;
                                  }
                                });
                              },
                              onTapPlus: () {
                                setState(() {
                                  if (secondUnit < 9) {
                                    secondUnit++;
                                  }
                                });
                              }),
                        ),
                      ])
                    ],
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                      child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.clear,
                            color: k_neonLightGreen,
                            size: 40,
                          ))),
                  Expanded(
                      child: InkWell(
                          onTap: () {
                            widget.onAccept((minuteTens * 10) + minuteUnit,
                                (secondTens * 10) + secondUnit);
                            Navigator.pop(context);
                          },
                          child: Icon(Icons.check,
                              color: k_neonLightGreen, size: 40)))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
