import 'package:flutter/material.dart';
import 'package:training_timer/constants/k_colors.dart';
import 'package:training_timer/general_components/gradient_border_container.dart';
import 'package:training_timer/general_components/inc_dec_button.dart';

class QuantityPopup extends StatefulWidget {
  Function(int) onAccept;
  String title;

  QuantityPopup({
    this.title = "",
    this.onAccept,
  });

  @override
  _QuantityPopupState createState() => _QuantityPopupState();
}

class _QuantityPopupState extends State<QuantityPopup> {
  int quantityTens = 0;
  int quantityUnit = 0;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
      child: GradientBorderContainer(
        backgroundColor: k_black,
        borderColor: LinearGradient(colors: [k_neonViolete, k_neonGreen]),
        height: 350,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Center(
                  child: Text(
                widget.title,
                style: TextStyle(color: k_neonLightGreen, fontSize: 25),
              )),
              Column(children: [
                Text("Cantidad",
                    style: TextStyle(color: k_neonLightGreen, fontSize: 15)),
                SizedBox(
                  height: 10,
                ),
                Row(children: [
                  SizedBox(
                    height: 200,
                    child: IncDecButton(
                        backgroundColor: k_black,
                        iconColor: k_neonLightGreen,
                        text: quantityTens.toString(),
                        style: TextStyle(color: k_neonLightGreen, fontSize: 25),
                        borderColor: LinearGradient(
                            colors: [k_neonViolete, k_neonGreen]),
                        onTapMinus: () {
                          setState(() {
                            if (quantityTens > 0) {
                              quantityTens--;
                            }
                          });
                        },
                        onTapPlus: () {
                          setState(() {
                            if (quantityTens < 9) {
                              quantityTens++;
                            }
                          });
                        }),
                  ),
                  SizedBox(
                    height: 200,
                    child: IncDecButton(
                        backgroundColor: k_black,
                        iconColor: k_neonLightGreen,
                        text: quantityUnit.toString(),
                        style: TextStyle(color: k_neonLightGreen, fontSize: 25),
                        borderColor: LinearGradient(
                            colors: [k_neonViolete, k_neonGreen]),
                        onTapMinus: () {
                          setState(() {
                            if (quantityUnit > 0) {
                              quantityUnit--;
                            }
                          });
                        },
                        onTapPlus: () {
                          setState(() {
                            if (quantityUnit < 9) {
                              quantityUnit++;
                            }
                          });
                        }),
                  ),
                ])
              ]),
              Row(
                children: [
                  Expanded(
                      child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Icon(
                            Icons.clear,
                            color: k_neonLightGreen,
                            size: 40,
                          ))),
                  Expanded(
                      child: InkWell(
                          onTap: () {
                            widget.onAccept((quantityTens * 10) + quantityUnit);
                            Navigator.pop(context);
                          },
                          child: Icon(Icons.check,
                              color: k_neonLightGreen, size: 40)))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
