import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:training_timer/constants/k_colors.dart';
import 'package:training_timer/general_components/custom_container.dart';
import 'package:training_timer/models/routine_item_model.dart';
import 'package:training_timer/providers/main_provider.dart';
import 'package:training_timer/screen_logic/routine_deatil_screen_logic.dart';

class RoutineDetailPage extends StatefulWidget {
  RoutineDetailPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RoutineDetailPageState createState() => _RoutineDetailPageState();
}

class _RoutineDetailPageState extends State<RoutineDetailPage> {
  double conteinerHeight = 0;
  int oldExceriseQuantity = 0;

  @override
  void initState() {
    super.initState();
    oldExceriseQuantity =
        Provider.of<MainProvider>(context, listen: false).exercisePerCircuit;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      backgroundColor: k_black,
      body: Column(children: [
        _header(),
        _body(),
        _footer(),
      ]),
    ));
  }

  _header() {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: IconButton(
                onPressed: () {
                  Provider.of<MainProvider>(context, listen: false)
                      .addRoutineItem(RoutineItemModel(
                    restMinutes:
                        Provider.of<MainProvider>(context, listen: false)
                            .restMinutes,
                    restSeconds:
                        Provider.of<MainProvider>(context, listen: false)
                            .restSeconds,
                    trainingMinutes:
                        Provider.of<MainProvider>(context, listen: false)
                            .trainingMinutes,
                    trainingSeconds:
                        Provider.of<MainProvider>(context, listen: false)
                            .trainingSeconds,
                  ));
                },
                icon: Icon(
                  Icons.expand_less,
                  color: k_neonViolete,
                  size: 65,
                )),
          ),
          Expanded(
              child: Center(
            child: Text(
                '${Provider.of<MainProvider>(context, listen: true).exercisePerCircuit} Ejercicios',
                style: TextStyle(color: k_neonLightGreen, fontSize: 25)),
          )),
          Expanded(
            child: IconButton(
                onPressed: () {
                  Provider.of<MainProvider>(context, listen: false)
                      .removeLastRoutineItem();
                },
                icon: Icon(
                  Icons.expand_more,
                  color: k_neonViolete,
                  size: 65,
                )),
          ),
        ]);
  }

  _body() {
    return Expanded(
        child: ListView.builder(
            itemCount: Provider.of<MainProvider>(context, listen: true)
                .routine
                .routineItems
                .length,
            itemBuilder: (BuildContext context, int index) {
              return _listViewItem(index);
            }));
  }

  _footer() {
    return Row(children: [
      Expanded(
          child: InkWell(
              onTap: () {
                RoutineScreenDetailLogic.onCancelCustomRoutineConfig(
                    context, oldExceriseQuantity);
                Navigator.pop(context);
              },
              child: Icon(
                Icons.clear,
                color: k_neonLightGreen,
                size: 40,
              ))),
      Expanded(
          child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.check, color: k_neonLightGreen, size: 40)))
    ]);
  }

  _listViewItem(int index) {
    return Row(children: [
      _listViewItemRowCell(index, "Ejercicio", () {
        RoutineScreenDetailLogic.callTrainingTimePopUp(context, index);
      },
          context
              .watch<MainProvider>()
              .routine
              .routineItems[index]
              .trainingMinutes,
          context
              .watch<MainProvider>()
              .routine
              .routineItems[index]
              .trainingSeconds),
      _listViewItemRowCell(index, "Descanso", () {
        RoutineScreenDetailLogic.callRestTimePopUp(context, index);
      },
          context.watch<MainProvider>().routine.routineItems[index].restMinutes,
          context
              .watch<MainProvider>()
              .routine
              .routineItems[index]
              .restSeconds),
    ]);
  }

  _listViewItemRowCell(
      int index, String title, Function onTap, int minutes, int seconds) {
    return Expanded(
      child: CustomContainer(
        height: 100,
        title: "Ejercicio",
        subTitle: '$minutes:$seconds',
        onTap: () {
          onTap();
        },
        backgroundColor: k_black,
        borderColor: LinearGradient(colors: [k_neonViolete, k_neonGreen]),
        textStyle: TextStyle(color: k_neonLightGreen, fontSize: 25),
      ),
    );
  }
}
