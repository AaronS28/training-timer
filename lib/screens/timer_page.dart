import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:training_timer/constants/k_colors.dart';
import 'package:training_timer/providers/main_provider.dart';
import 'package:training_timer/screen_logic/timer_screen_logic.dart';
import 'package:minimize_app/minimize_app.dart';

class TimerPage extends StatefulWidget {
  TimerPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TimerPageState createState() => _TimerPageState();
}

class _TimerPageState extends State<TimerPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        MinimizeApp.minimizeApp();
        return false;
      },
      child: SafeArea(
          child: Scaffold(
              backgroundColor: k_black,
              body: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: Text(
                      '${context.watch<MainProvider>().currentStage}',
                      style: TextStyle(color: k_neonGreen, fontSize: 50),
                    ),
                  ),
                  Center(
                    child: Text(
                      '${context.watch<MainProvider>().currentMinuteTimer}:${context.watch<MainProvider>().currentSecondTimer}',
                      style: TextStyle(color: k_neonGreen, fontSize: 130),
                    ),
                  ),
                  Slider(
                    min: 0,
                    max: (context
                                .watch<MainProvider>()
                                .currentMaxTimeInSeconds !=
                            0)
                        ? context.watch<MainProvider>().currentMaxTimeInSeconds
                        : 1,
                    value:
                        context.watch<MainProvider>().currentProgressInSeconds,
                    activeColor: k_neonViolete,
                    inactiveColor: k_neonLightGreen,
                    onChanged: (vble) {},
                  ),
                  Center(
                    child: IconButton(
                      iconSize: 70,
                      onPressed: () async {
                        await _startTimer();
                      },
                      icon:
                          Icon(Icons.play_arrow, color: k_neonGreen, size: 70),
                    ),
                  )
                ],
              ))),
    );
  }

  _startTimer() async {
    await TimerScreenLogic.startPreparationTime(context);
    await TimerScreenLogic.startTraining(context);
  }
}
