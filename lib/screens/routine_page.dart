import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:training_timer/constants/k_colors.dart';
import 'package:training_timer/general_components/custom_container.dart';
import 'package:training_timer/general_components/gradient_border_container.dart';
import 'package:training_timer/providers/main_provider.dart';
import 'package:training_timer/screen_logic/routine_screen_logic.dart';

class RoutinePage extends StatefulWidget {
  RoutinePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RoutinePageState createState() => _RoutinePageState();
}

class _RoutinePageState extends State<RoutinePage> {
  double conteinerHeight = 0;

  @override
  Widget build(BuildContext context) {
    conteinerHeight = (MediaQuery.of(context).size.height - 120) / 4;
    return SafeArea(
      child: Scaffold(
        backgroundColor: k_black,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            CustomContainer(
              width: double.infinity,
              height: conteinerHeight,
              title: "Preparacion",
              subTitle:
                  '${context.watch<MainProvider>().preparationMinutes}:${context.watch<MainProvider>().preparationSeconds}',
              onTap: () {
                RoutineScreenLogic.callPreparationTimePopUp(context);
              },
              backgroundColor: k_black,
              borderColor: LinearGradient(colors: [k_neonViolete, k_neonGreen]),
              textStyle: TextStyle(color: k_neonLightGreen, fontSize: 25),
            ),
            CustomContainer(
              width: double.infinity,
              height: conteinerHeight,
              title: "Ejercicio",
              subTitle:
                  '${context.watch<MainProvider>().trainingMinutes}:${context.watch<MainProvider>().trainingSeconds}',
              onTap: () {
                RoutineScreenLogic.callTrainingTimePopUp(context);
              },
              backgroundColor: k_black,
              borderColor: LinearGradient(colors: [k_neonViolete, k_neonGreen]),
              textStyle: TextStyle(color: k_neonLightGreen, fontSize: 25),
            ),
            CustomContainer(
              width: double.infinity,
              height: conteinerHeight,
              title: "Descanso",
              subTitle:
                  '${context.watch<MainProvider>().restMinutes}:${context.watch<MainProvider>().restSeconds}',
              onTap: () {
                RoutineScreenLogic.callRestTimePopUp(context);
              },
              backgroundColor: k_black,
              borderColor: LinearGradient(colors: [k_neonViolete, k_neonGreen]),
              textStyle: TextStyle(color: k_neonLightGreen, fontSize: 25),
            ),
            SizedBox(
              height: conteinerHeight,
              child: Row(
                children: [
                  Expanded(
                      child: InkWell(
                    onTap: () {
                      RoutineScreenLogic.callExerciseQuantityPopUp(context);
                    },
                    child: GradientBorderContainer(
                        backgroundColor: k_black,
                        borderColor: LinearGradient(
                            colors: [k_neonViolete, k_neonGreen]),
                        height: conteinerHeight,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              color: k_black,
                              height: 65,
                              width: 65,
                              child: IconButton(
                                icon: Icon(
                                  Icons.format_list_bulleted,
                                  color: k_neonLightGreen,
                                  size: 65,
                                ),
                                onPressed: () {
                                  RoutineScreenLogic.onCreateCustomRoutineTap(
                                      context);
                                  Navigator.pushNamed(
                                      context, '/routineDetail');
                                },
                              ),
                            ),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                      '${context.watch<MainProvider>().exercisePerCircuit}',
                                      style: TextStyle(
                                          color: k_neonLightGreen,
                                          fontSize: 25)),
                                  Text("Ejercicios",
                                      style: TextStyle(
                                          color: k_neonLightGreen,
                                          fontSize: 25)),
                                ],
                              ),
                            )
                          ],
                        )),
                  )),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        RoutineScreenLogic.callCircuitQuantityPopUp(context);
                      },
                      child: GradientBorderContainer(
                          backgroundColor: k_black,
                          borderColor: LinearGradient(
                              colors: [k_neonViolete, k_neonGreen]),
                          height: conteinerHeight,
                          child: Column(
                            children: [
                              Container(
                                color: k_black,
                                height: 65,
                                width: 65,
                                child: IconButton(
                                  icon: Icon(
                                    Icons.query_builder,
                                    color: k_neonLightGreen,
                                    size: 65,
                                  ),
                                  onPressed: () {
                                    RoutineScreenLogic
                                        .callRestTimeBetweenCircuitPopUp(
                                            context);
                                  },
                                ),
                              ),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                        '${context.watch<MainProvider>().circuitQuantity}',
                                        style: TextStyle(
                                            color: k_neonLightGreen,
                                            fontSize: 25)),
                                    Text("Circuitos",
                                        style: TextStyle(
                                            color: k_neonLightGreen,
                                            fontSize: 25)),
                                  ],
                                ),
                              )
                            ],
                          )),
                    ),
                  )
                ],
              ),
            ),
            Center(
              child: IconButton(
                iconSize: 70,
                icon: Icon(Icons.play_arrow, color: k_neonGreen, size: 70),
                onPressed: () {
                  if (Provider.of<MainProvider>(context, listen: false)
                          .routine ==
                      null) {
                    var result = RoutineScreenLogic.callRoutineBuilder(context);
                    Provider.of<MainProvider>(context, listen: false)
                        .setRoutine(result);
                  }
                  Provider.of<MainProvider>(context, listen: false)
                      .setRoutineList();
                  Navigator.pushNamed(context, '/timer');
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
