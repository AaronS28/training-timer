class RoutineItemModel {
  int trainingMinutes = 0;
  int trainingSeconds = 0;
  int restMinutes = 0;
  int restSeconds = 0;

  RoutineItemModel(
      {this.restMinutes,
      this.restSeconds,
      this.trainingMinutes,
      this.trainingSeconds});
}
