import 'package:training_timer/models/routine_item_model.dart';

class RoutineModel {
  List<RoutineItemModel> routineItems;
  int restRoutineMinutes = 0;
  int restRoutineSeconds = 0;
  RoutineModel(
      {this.restRoutineMinutes, this.restRoutineSeconds, this.routineItems});
}
